package com.sourceit.data;

import com.sourceit.model.Smartphone;
import com.sourceit.model.Store;

public final class Generator {
    private Generator() {
    }
    public static Store[] generate() {
        Store[] stores = new Store[3];
        {
            Smartphone[] smartphone = {
                    new Smartphone("Iphone", "XS", 19600),
                    new Smartphone("Samsung", "S20", 25000),
                    new Smartphone("Huawei", "P40", 24500),
                    new Smartphone("Xiaomi", "P30Pro", 17900)
            };
            stores[0] = new Store("Comfy", "г.Харьков, ТЦ Французский Бульвар, ул. Академика Павлова, 44", smartphone);
        }
        {
            Smartphone[] smartphone = {
                    new Smartphone("Iphone", "XS", 20100),
                    new Smartphone("Samsung", "S20", 26100),
                    new Smartphone("Huawei", "P40L", 10000),
                    new Smartphone("Xiaomi", "P30 Pro", 16000),
                    new Smartphone("Iphone", "11 PRO", 29800)
            };
            stores[1] = new Store("Citrus", "вулиця Героїв Праці, 7, Харків", smartphone);
        }
        {
            Smartphone[] smartphone = {
                    new Smartphone("Xiaomi", "V20", 10000),
                    new Smartphone("Samsung", "M30s", 5000),
                    new Smartphone("Xiaomi", "P10", 7800)
            };
            stores[2] = new Store("mobilka", "вулиця Героїв Праці, 27, Харків", smartphone);
        }
        return stores;
    }
}





