package com.sourceit;

import com.sourceit.data.Generator;
import com.sourceit.model.StoreManager;

import java.util.Scanner;

public class Task {
    public void run() {
        StoreManager storeManager = new StoreManager(Generator.generate());


        Scanner scan = new Scanner(System.in);
        System.out.println("Все доступные сматфоны какого производителя Вы желаете найти? (Iphone, Samsung, Xiaomi, Huawei): ");
        String desiredManufacturer = scan.nextLine();
        System.out.println("Самый дешёвый смартфон какой модели Вы желаете найти? (Iphone: XS , 11 Pro ; Samsung:  S20, M30S ; Huawei: P40, P40L; Xiaomi: P30Pro, V20, P10): ");
        String desiredModel = scan.nextLine();

        storeManager.findAndPrintSmartphoneByManufacturer(desiredManufacturer);
        storeManager.findAndPrintCheapestSmartphoneByModel(desiredModel);
        storeManager.findAndPrindCheapestSmartphone();
    }
}


