package com.sourceit.model;

public class Store {
    private String name;
    private String address;
    private Smartphone[] smartphones;

    public Store(String name, String address, Smartphone[] smartphones) {
        this.name = name;
        this.address = address;
        this.smartphones = smartphones;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public void findSmartphoneByManufacturer(String manufacturer) {
        for (Smartphone smartphone : smartphones) {
            if (smartphone.getManufacturer().equalsIgnoreCase(manufacturer)) {
                PrintInformation.printSmartphonesByManufacturer(smartphone, manufacturer, name, address);
            }
        }
    }

    public Smartphone cheapestSmartphoneByModel(int lowerPrice, String modelName) {
        Smartphone cheapestsmartphoneByModel = null;
        for (Smartphone cheap : smartphones) {
            if (cheap.getPrice() < lowerPrice && cheap.getModel().equalsIgnoreCase(modelName)) {
                lowerPrice = cheap.getPrice();
                cheapestsmartphoneByModel = cheap;
            }
        }
        return cheapestsmartphoneByModel;
    }

    public Smartphone cheapestSmart(int lowestSmartPrice) {
            Smartphone cheapestSmartphoneInStore = null;
            for (Smartphone smart : smartphones) {
                if (smart.getPrice() < lowestSmartPrice) {
                    lowestSmartPrice = smart.getPrice();
                            cheapestSmartphoneInStore = smart;
                }
            }
            return cheapestSmartphoneInStore;
        }
    }