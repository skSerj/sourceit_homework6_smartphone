package com.sourceit.model;

public class StoreManager {

    private final static int BIG_INT = Integer.MAX_VALUE;

    Store[] stores;

    public StoreManager(Store[] stores) {
        this.stores = stores;
    }

    public void findAndPrintCheapestSmartphoneByModel(String modelName) {
        int lowestPrice = BIG_INT;
        Store cheapestStore = getFirsrStoreWithFinderModel(modelName);
        if (cheapestStore == null) {
            System.err.println("Смартфона нет в наличии");
            return;
        }

        Smartphone cheapSmartInShops = cheapestStore.cheapestSmartphoneByModel(BIG_INT, modelName);
        for (Store store : stores) {
            Smartphone cheapestSmartInShopByModel = store.cheapestSmartphoneByModel(lowestPrice, modelName);
            if (cheapestSmartInShopByModel != null) {
                cheapSmartInShops = cheapestSmartInShopByModel;
                cheapestStore = store;
                lowestPrice = cheapestSmartInShopByModel.getPrice();
            }
        }
        PrintInformation.printCheapestSmartphoneByModel(cheapSmartInShops, cheapestStore);
    }

    private Store getFirsrStoreWithFinderModel(String modelName) {
        for (Store store : stores) {
            if (store.cheapestSmartphoneByModel(BIG_INT, modelName) != null) {
                return store;

            }
        }
        return null;
    }

    public void findAndPrintSmartphoneByManufacturer(String desiredManufacturer) {
        for (Store store : stores) {
            store.findSmartphoneByManufacturer(desiredManufacturer);
        }
    }

    public void findAndPrindCheapestSmartphone() {
        int lowestSmartPrice = BIG_INT;
        Store cheapestStoreWithSmart = null;
        assert false;
        Smartphone cheapSmartInShops = null;
        for (Store store : stores) {
            Smartphone cheapestSmartInShop = store.cheapestSmart(lowestSmartPrice);
            if (cheapestSmartInShop != null) {
                cheapSmartInShops = cheapestSmartInShop;
                cheapestStoreWithSmart = store;
               lowestSmartPrice = cheapestSmartInShop.getPrice();
            }
        }
        PrintInformation.printCheapestSmartInShops(cheapestStoreWithSmart,cheapSmartInShops);
    }
}



