package com.sourceit.model;

public final class PrintInformation {
    private PrintInformation() {
    }

    public static void printCheapestSmartphoneByModel(Smartphone cheapSmartInShops, Store cheapestStore) {
        System.out.println("Смартфон, указанной Вами модели, с наиболее низкой ценой: " + cheapSmartInShops.getManufacturer() + " " + cheapSmartInShops.getModel() + ", цена: " + cheapSmartInShops.getPrice() + ". Доступен в магазине: " + cheapestStore.getName() + ", адрес:" + cheapestStore.getAddress());
    }

    public static void printSmartphonesByManufacturer(Smartphone smartphone, String manufacturer, String nameStore, String addressStore) {
        System.out.println((String.format("ваш сматфон: %s %s", manufacturer, smartphone.getModel())) + ", по цене: " + smartphone.getPrice() + ", в магазине: " + nameStore + ", по адресу: " + addressStore);

    }

    public static void printCheapestSmartInShops(Store cheapestStoreWithSmart, Smartphone cheapSmartInShops) {
        System.out.println("Cамый дешёвый из всех смартфон: " + cheapSmartInShops.getManufacturer() + " " + cheapSmartInShops.getModel()+ " цена: "+ cheapSmartInShops.getPrice() + ". Доступен в: " + cheapestStoreWithSmart.getName() + ", по адресу: " + cheapestStoreWithSmart.getAddress());
    }
}


